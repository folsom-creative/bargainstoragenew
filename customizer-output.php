<?php

?>
<style type="text/css">
.primary-color {
	color: <?php echo esc_attr( get_theme_mod( 'bargain_primary_color', '#63a7b6' ) ); ?>;
}

.primary-background {
	background-color: <?php echo esc_attr( get_theme_mod( 'bargain_primary_color', '#63a7b6' ) ); ?>;
}

.info-color {
	color: <?php echo esc_attr( get_theme_mod( 'bargain_info_color', '#63a7b6' ) ); ?>;
}

.info-background {
	background-color: <?php echo esc_attr( get_theme_mod( 'bargain_info_color', '#63a7b6' ) ); ?>;
}

.secondary-color {
	color: <?php echo esc_attr( get_theme_mod( 'bargain_secondary_color', '#c75335' ) ); ?>;
}

.secondary-background {
	background-color: <?php echo esc_attr( get_theme_mod( 'bargain_secondary_color', '#c75335' ) ); ?>;
}

.black-color {
	color: <?php echo esc_attr( get_theme_mod( 'bargain_black_color', '#282828' ) ); ?>;
}

.black-background {
	background-color: <?php echo esc_attr( get_theme_mod( 'bargain_black_color', '#282828' ) ); ?>;
}

.charcoal-color {
	color: <?php echo esc_attr( get_theme_mod( 'bargain_charcoal_color', '#4b4b4b' ) ); ?>;
}

.charcoal-background {
	background-color: <?php echo esc_attr( get_theme_mod( 'bargain_charcoal_color', '#4b4b4b' ) ); ?>;
}

.darkgrey-color {
	color: <?php echo esc_attr( get_theme_mod( 'bargain_darkgrey_color', '#7d7d7d' ) ); ?>;
}

.darkgrey-background {
	background-color: <?php echo esc_attr( get_theme_mod( 'bargain_darkgrey_color', '#7d7d7d' ) ); ?>;
}


.grey-color {
	color: <?php echo esc_attr( get_theme_mod( 'bargain_grey_color', '#b5b5b5' ) ); ?>;
}

.grey-background {
	background-color: <?php echo esc_attr( get_theme_mod( 'bargain_grey_color', '#b5b5b5' ) ); ?>;
}

.site {
	background: <?php echo esc_attr( get_theme_mod( 'bargain_page_color', '' ) ); ?>;
}

.lightgrey-color {
	color: <?php echo esc_attr( get_theme_mod( 'bargain_lightgrey_color', '#e9e9e9' ) ); ?>;
}

.lightgrey-background {
	background-color: <?php echo esc_attr( get_theme_mod( 'bargain_lightgrey_color', '#e9e9e9' ) ); ?>;
}

.white-color {
	color: <?php echo esc_attr( get_theme_mod( 'bargain_white_color', '#ffffff' ) ); ?>;
}

.white-background {
	background-color: <?php echo esc_attr( get_theme_mod( 'bargain_white_color', '#ffffff' ) ); ?>;
}

body {
	background-color: <?php echo esc_attr( get_theme_mod( 'bargain_grey_color', '#b5b5b5' ) ); ?>;
}
a {
	color: <?php echo esc_attr( get_theme_mod( 'bargain_primary_color', '#63a7b6' ) ); ?>;
}

.site-name {
	color: <?php echo esc_attr( get_theme_mod( 'bargain_black_color', '#282828' ) ); ?>;
}
.name .site-name {
	color: <?php echo esc_attr( get_theme_mod( 'bargain_white_color', '#ffffff' ) ); ?>;
}
.toggle-panel {
	color: <?php echo esc_attr( get_theme_mod( 'bargain_black_color', '#282828' ) ); ?>;
}

/* features widget */
.feature-widget .feature-widget-title {
	color: <?php echo esc_attr( get_theme_mod( 'bargain_black_color', '#282828' ) ); ?>;
	border-color: <?php echo esc_attr( get_theme_mod( 'bargain_grey_color', '#b5b5b5' ) ); ?>;
}

/* vert nav settings */
.vert-prev, .vert-next {
	color: <?php echo esc_attr( get_theme_mod( 'bargain_grey_color', '#b5b5b5' ) ); ?>;
	background-color: <?php echo esc_attr( get_theme_mod( 'bargain_lightgrey_color', '#e9e9e9' ) ); ?>;
}

.vert-prev .triangle {
	border-bottom-color: <?php echo esc_attr( get_theme_mod( 'bargain_grey_color', '#b5b5b5' ) ); ?> !important;
}

.vert-next .triangle {
	border-top-color: <?php echo esc_attr( get_theme_mod( 'bargain_grey_color', '#b5b5b5' ) ); ?> !important;
}


ul li:before {
	color: <?php echo esc_attr( get_theme_mod( 'bargain_secondary_color', '#c75335' ) ); ?>;
}

ul li ul li:before {
	color: <?php echo esc_attr( get_theme_mod( 'bargain_charcoal_color', '#4b4b4b' ) ); ?>;
}

.scotch-panel-right {
	background-color: <?php echo esc_attr( get_theme_mod( 'bargain_black_color', '#282828' ) ); ?>;
}
.mobile-navigation a, .mobile-navigation a:hover {
	color: <?php echo esc_attr( get_theme_mod( 'bargain_white_color', '#ffffff' ) ); ?>;
}

.copyright {
	color: <?php echo esc_attr( get_theme_mod( 'bargain_white_color', '#ffffff' ) ); ?>;
}
.site-footer {
	background-color: <?php echo esc_attr( get_theme_mod( 'bargain_black_color', '#282828' ) ); ?>;
}
.site-footer .button {
	background-color: <?php echo esc_attr( get_theme_mod( 'bargain_secondary_color', '#c75335' ) ); ?>;
}

</style>



















