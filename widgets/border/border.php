<?php

class bargain_border extends WP_Widget {

	public function __construct() {

		parent::__construct(
			'border',
			__( 'Border', 'bargain' ),
			array(
				'description' => __( 'Adds border between page sections.', 'bargain' ),
				'classname'   => 'border-widget',
			)
		);

	}

	//output on the front end
	public function widget( $args, $instance ) {
		include( plugin_dir_path(__FILE__) . 'border-output.php' );
	}

	public function form( $instance ) {
		// Set default values
		$instance = wp_parse_args( (array) $instance, array(
			'border_type' => ''
		) );

		// Retrieve an existing value from the database
		$border_type = !empty( $instance['border_type'] ) ? $instance['border_type'] : '';


		// Form fields
		echo '<p>';
		echo '	<label for="' . $this->get_field_id( 'border_type' ) . '" class="border_type_label">' . __( 'Select a border type:', 'bargain' ) . '</label>';
		echo '	<select id="' . $this->get_field_id( 'border_type' ) . '" name="' . $this->get_field_name( 'border_type' ) . '" class="widefat">';
		echo '		<option value="top" ' . selected( $border_type, 'top', false ) . '> ' . __( 'Top', 'bargain' );
		echo '		<option value="bottom" ' . selected( $border_type, 'bottom', false ) . '> ' . __( 'Bottom', 'bargain' );
		echo '	</select>';
		echo '</p>';

	}

	public function update( $new_instance, $old_instance ) {

		$instance = $old_instance;

		$instance['border_type'] = !empty( $new_instance['border_type'] ) ? strip_tags( $new_instance['border_type'] ) : '';

		return $instance;

	}

}

function bargain_register_border_widgets() {
	register_widget( 'bargain_border' );
}
add_action( 'widgets_init', 'bargain_register_border_widgets' );