<?php
/**
 * Border Widget
 */
require get_template_directory() . '/widgets/border/border.php';

/**
 * Section Title Widget
 */
require get_template_directory() . '/widgets/section-title/section-title.php';