<?php

class bargain_title extends WP_Widget {

	public function __construct() {

		parent::__construct(
			'bargain_title',
			__( 'Section Title', 'bargain' ),
			array(
				'description' => __( 'Adds a title section to the page', 'bargain' ),
				'classname'   => 'title-widget',
			)
		);

	}

	//output on the front end
	public function widget( $args, $instance ) {
		include( plugin_dir_path(__FILE__) . 'section-title-output.php' );
	}

	public function form( $instance ) {
		// Set default values
		$instance = wp_parse_args( (array) $instance, array(
			'bargain_title' => '',
			'title_size' => '',
			'title_align' => ''
		) );

		// Retrieve an existing value from the database
		$bargain_title = !empty( $instance['title_color'] ) ? $instance['bargain_title'] : '';
		$title_size = !empty( $instance['title_size'] ) ? $instance['title_size'] : '';
		$title_align = !empty( $instance['title_align'] ) ? $instance['title_align'] : '';


		// Form fields
		echo '<p>';
		echo '	<label for="' . $this->get_field_id( 'bargain_title' ) . '" class="bargain_title_label">' . __( 'Title', 'bargain' ) . '</label>';
		echo '	<input type="text" id="' . $this->get_field_id( 'bargain_title' ) . '" name="' . $this->get_field_name( 'bargain_title' ) . '" class="widefat" placeholder="' . esc_attr__( '', 'bargain' ) . '" value="' . esc_attr( $bargain_title ) . '">';
		echo '</p>';

		echo '<p>';
		echo '	<label for="' . $this->get_field_id( 'title_size' ) . '" class="title_color_label">' . __( 'Text Size:', 'bargain' ) . '</label>';
		echo '	<select id="' . $this->get_field_id( 'title_size' ) . '" name="' . $this->get_field_name( 'title_size' ) . '" class="widefat">';
		echo '		<option value="regular" ' . selected( $title_size, 'regular', false ) . '> ' . __( 'Regular', 'bargain' );
		echo '		<option value="large" ' . selected( $title_size, 'large', false ) . '> ' . __( 'Large', 'bargain' );
		echo '		<option value="xlarge" ' . selected( $title_size, 'xlarge', false ) . '> ' . __( 'Extra Large', 'bargain' );
		echo '	</select>';
		echo '</p>';

		echo '<p>';
		echo '	<label for="' . $this->get_field_id( 'title_align' ) . '" class="title_color_label">' . __( 'Text Alignment:', 'bargain' ) . '</label>';
		echo '	<select id="' . $this->get_field_id( 'title_align' ) . '" name="' . $this->get_field_name( 'title_align' ) . '" class="widefat">';
		echo '		<option value="center" ' . selected( $title_align, 'center', false ) . '> ' . __( 'Center', 'bargain' );
		echo '		<option value="left" ' . selected( $title_align, 'left', false ) . '> ' . __( 'Left', 'bargain' );
		echo '		<option value="right" ' . selected( $title_align, 'right', false ) . '> ' . __( 'Right', 'bargain' );
		echo '	</select>';
		echo '</p>';

	}

	public function update( $new_instance, $old_instance ) {

		$instance = $old_instance;

		$instance['bargain_title'] = !empty( $new_instance['bargain_title'] ) ? strip_tags( $new_instance['bargain_title'] ) : '';
		$instance['title_size'] = !empty( $new_instance['title_size'] ) ? strip_tags( $new_instance['title_size'] ) : '';
		$instance['title_align'] = !empty( $new_instance['title_align'] ) ? strip_tags( $new_instance['title_align'] ) : '';

		return $instance;

	}

}

function bargain_register_title_widgets() {
	register_widget( 'bargain_title' );
}
add_action( 'widgets_init', 'bargain_register_title_widgets' );