(function( $ ) {
    function triangleRezise(){
       $('.triangle').each(function(){
            var parentWidth = $(this).parent().width();
            var parentHeight = $(this).parent().height();
            var widthVariable = $(this).attr('w');

            if(!widthVariable){ var widthVariable = $(this).attr('h');}
            triangleWidth = parseInt(widthVariable) / 100 * parentWidth;
            var heightVariable = $(this).attr('h');
            if(!heightVariable){ var heightVariable = $(this).attr('w');}
            triangleHeight = parseInt(heightVariable) / 300 * parentWidth;


            if($(this).hasClass('down')) {
                triangleWidth = triangleWidth / 2;
                triangleHeight = "" + triangleHeight + "px";
                triangleWidth = "" + triangleWidth + "px";
                var triangle = triangleHeight + " " + triangleWidth + " " + "0px " + triangleWidth;
                $(this).css("border-width", triangle);
            }
            else{

            };
        });
    };
    triangleRezise();
    $(window).resize(function() {
      triangleRezise()
    });

    $('.reviews-carousel').slick({
      dots: true,
      arrows: false,
      autoplay: true,
      autoplaySpeed: 3500,
      infinite: true,
      speed: 300,
      slidesToShow: 3,
      slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1,
            infinite: true,
            dots: true
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ]
    });

    $('.vertical-gallery').slick({
      dots: false,
      arrows: true,
      autoplay: false,
      infinite: true,
      speed: 300,
      slidesToShow: 2,
      slidesToScroll: 2,
      vertical: true,
      verticalSwiping: true,
      prevArrow: '<div class="vert-prev"><span class="triangle"></span></div>',
      nextArrow: '<div class="vert-next"><span class="triangle"></span></div>'
    });

    $('.hide-parent').closest('.panel-grid-cell').addClass('hidden-mobile');

    var scotchPanel = $('#panel-example').scotchPanel({
        containerSelector: 'body',
        direction: 'right',
        duration: 300,
        transition: 'ease',
        clickSelector: '.toggle-panel',
        distanceX: '75%',
        enableEscapeKey: true
    });

    $(window).resize(function() {
        if ($(window).width() >= 769 && $('.scotch-panel-canvas').hasClass('scotch-is-showing')) {
            scotchPanel.close();
        }
    });

    $('.match').matchHeight({
        byRow: false,
        property: 'min-height',
        target: null,
        remove: false
    });


})( jQuery )