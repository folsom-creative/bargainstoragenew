<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Bargain_2
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="site-info">
			<?php if ( get_theme_mod( 'bargain_footer_logo' ) ) : ?>
		    <div class="logo">
		        <a href='<?php echo esc_url( home_url( '/' ) ); ?>' title='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>' rel='home'><img src='<?php echo esc_url( get_theme_mod( 'bargain_footer_logo' ) ); ?>' alt='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>'></a>
		    </div>
			<?php else : ?>
				<!-- no logo set -->
			<?php endif; ?>
			 <div class="social">
	            <a href="https://www.facebook.com/bargainstorage/" target="_blank"><i class="fa fa-facebook"></i></a>
	            <a href="https://twitter.com/bargain_storage" target="_blank"><i class="fa fa-twitter"></i></a>
	            <a href="https://www.youtube.com/channel/UCKHnyMpBaZXClVUtRNdYxSA" target="_blank"><i class="fa fa-youtube"></i></a>
	            <a href="https://instagram.com/bargainstorage/" target="_blank"><i class="fa fa-instagram"></i></a>
	        </div>
			<div class="copyright">Copyright &copy; <?php echo date('Y'); ?> <?php bloginfo( 'description' ); ?></div>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
