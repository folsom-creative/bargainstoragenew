<?php

if(isset($instance['title_size'])) {
    $instance['title_size'];
}
if(isset($instance['title_align'])) {
    $instance['title_align'];
}


if ($instance['title_size'] == 'large') {
	$size = 'large';
} elseif ($instance['title_size'] == 'xlarge') {
	$size = 'xlarge';
};

if ($instance['title_align'] == 'left') {
	$align = 'text-left';
} elseif ($instance['title_align'] == 'right') {
	$align = 'text-right';
};

?>
<?php echo $args['before_widget']; ?>

<div class="section-title <?php echo $size; ?> <?php echo $align; ?>">
	<?php echo $instance['bargain_title']; ?>
</div>

<?php echo $args['after_widget']; ?>