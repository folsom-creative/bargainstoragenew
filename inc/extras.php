<?php
/**
 * Custom functions that act independently of the theme templates.
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package Bargain_2
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function bargain_body_classes( $classes ) {
	// Adds a class of group-blog to blogs with more than 1 published author.
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}

	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	return $classes;
}
add_filter( 'body_class', 'bargain_body_classes' );

// add options page
if( function_exists('acf_add_options_page') ) {
 
	$acf_options = acf_add_options_page(array(
		'page_title' 	=> 'Additional Settings',
		'menu_title' 	=> 'Addtl. Settings',
		'menu_slug' 	=> 'additional-settings',
		'capability' 	=> 'edit_posts',
		'redirect' 	=> false
	));
 
}

// Add Formats Dropdown Menu To MCE
if ( ! function_exists( 'wpex_style_select' ) ) {
	function wpex_style_select( $buttons ) {
		array_push( $buttons, 'styleselect' );
		return $buttons;
	}
}
add_filter( 'mce_buttons', 'wpex_style_select' );

/*
* Callback function to filter the MCE settings
*/

function my_mce_before_init_insert_formats( $init_array ) {  

// Define the style_formats array

	$style_formats = array(  
		// Each array child is a format with it's own settings
		array(  
			'title' => 'Primary Color',  
			'block' => 'p',  
			'classes' => 'primary-color',
			'wrapper' => false,
			
		),  
		array(  
			'title' => 'Secondary Color',  
			'block' => 'p',  
			'classes' => 'secondary-color',
			'wrapper' => false,
		),
		array(  
			'title' => 'Coffee Service',  
			'block' => 'p',  
			'classes' => 'coffee-service',
			'wrapper' => false,
		),
		array(  
			'title' => 'Century 731',  
			'block' => 'p',  
			'classes' => 'century731',
			'wrapper' => false,
		),
		array(  
			'title' => 'Open Sans',  
			'block' => 'p',  
			'classes' => 'opensans',
			'wrapper' => false,
		),
		array(  
			'title' => 'Uppercase',  
			'block' => 'span',  
			'classes' => 'uppercase',
			'wrapper' => true,
		),
	);  
	// Insert the array, JSON ENCODED, into 'style_formats'
	$init_array['style_formats'] = json_encode( $style_formats );  
	
	return $init_array;  
  
} 
// Attach callback to 'tiny_mce_before_init' 
add_filter( 'tiny_mce_before_init', 'my_mce_before_init_insert_formats' ); 
add_filter('acf/fields/wysiwyg/toolbars', 'my_mce_before_init_insert_formats');

// Enable font size & font family selects in the editor
if ( ! function_exists( 'wpex_mce_buttons' ) ) {
	function wpex_mce_buttons( $buttons ) {
		//array_unshift( $buttons, 'fontselect' ); // Add Font Select
		array_unshift( $buttons, 'fontsizeselect' ); // Add Font Size Select
		return $buttons;
	}
}
add_filter( 'mce_buttons_2', 'wpex_mce_buttons' );

// Customize mce editor font sizes
if ( ! function_exists( 'wpex_mce_text_sizes' ) ) {
	function wpex_mce_text_sizes( $initArray ){
		$initArray['fontsize_formats'] = "0.800rem 1rem 1.067rem 1.467rem 1.933rem";
		return $initArray;
	}
}
add_filter( 'tiny_mce_before_init', 'wpex_mce_text_sizes' );