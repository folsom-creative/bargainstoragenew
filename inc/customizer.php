<?php
/**
 * Bargain 2 Theme Customizer.
 *
 * @package Bargain_2
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function bargain_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';
	$wp_customize->remove_control( 'header_textcolor');
	$wp_customize->remove_control( 'background_color');
	$wp_customize->remove_section( 'background_image');
	$wp_customize->remove_section( 'header_image');



	$wp_customize->add_section( 'header' , array(
	    'title'      => __('Header','bargain'),
	    'priority'   => 30,
	) );

	$wp_customize->add_section( 'footer' , array(
	    'title'      => __('Footer','bargain'),
	    'priority'   => 31,
	) );

	// add header logo setting
	$wp_customize->add_setting( 'bargain_logo', array(
	'default' => '',
	'type' => 'theme_mod',
	'capability' => 'edit_theme_options',
	'transport' => '',
	) );
	// add header logo control
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'bargain_logo', array(
	    'label'    => __( 'Logo', 'bargain' ),
	    'section'  => 'title_tagline',
	    'settings' => 'bargain_logo',
	) ) );


	// add footer logo setting
	$wp_customize->add_setting( 'bargain_footer_logo', array(
	'default' => '',
	'type' => 'theme_mod',
	'capability' => 'edit_theme_options',
	'transport' => '',
	) );
	// add footer logo control
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'bargain_footer_logo', array(
	    'label'    => __( 'Footer Logo', 'bargain' ),
	    'section'  => 'title_tagline',
	    'settings' => 'bargain_footer_logo',
	) ) );

	// add primary color setting
	$wp_customize->add_setting(
	    'bargain_primary_color',
	    array(
	        'default'     => '#63a7b6'
	    )
	);
	//add primary color control
	$wp_customize->add_control(
	    new WP_Customize_Color_Control(
	        $wp_customize,
	        'bargain_primary_color',
	        array(
	            'label'      => __( 'Primary Color', 'bargain' ),
	            'section'    => 'colors',
	            'settings'   => 'bargain_primary_color',
	            'description' => 'Primary color used in the header, and primary highlights.'
	        )
	    )
	);

	// add header color setting
	$wp_customize->add_setting(
	    'bargain_header_color',
	    array(
	        'default'     => '#63a7b6'
	    )
	);
	//add header color control
	$wp_customize->add_control(
	    new WP_Customize_Color_Control(
	        $wp_customize,
	        'bargain_header_color',
	        array(
	            'label'      => __( 'Header Background Color', 'bargain' ),
	            'section'    => 'colors',
	            'settings'   => 'bargain_header_color',
	            'description' => 'Color used in the header, generally matches primary.'
	        )
	    )
	);

	// add navbar color setting
	$wp_customize->add_setting(
	    'bargain_navbar_color',
	    array(
	        'default'     => '#282828'
	    )
	);
	//add navbar color control
	$wp_customize->add_control(
	    new WP_Customize_Color_Control(
	        $wp_customize,
	        'bargain_navbar_color',
	        array(
	            'label'      => __( 'Navbar Background Color', 'bargain' ),
	            'section'    => 'colors',
	            'settings'   => 'bargain_navbar_color',
	            'description' => 'Color of the navbar'
	        )
	    )
	);

	// add header color setting
	$wp_customize->add_setting(
	    'bargain_info_color',
	    array(
	        'default'     => '#63a7b6'
	    )
	);
	//add header color control
	$wp_customize->add_control(
	    new WP_Customize_Color_Control(
	        $wp_customize,
	        'bargain_info_color',
	        array(
	            'label'      => __( 'Info Box Background Color', 'bargain' ),
	            'section'    => 'colors',
	            'settings'   => 'bargain_info_color',
	            'description' => 'Color in the sidebar infobox, generally matches primary.'
	        )
	    )
	);

	

	// add secondary color setting
	$wp_customize->add_setting(
	    'bargain_secondary_color',
	    array(
	        'default'     => '#c75335'
	    )
	);
	//add secondary color control
	$wp_customize->add_control(
	    new WP_Customize_Color_Control(
	        $wp_customize,
	        'bargain_secondary_color',
	        array(
	            'label'      => __( 'Secondary Color', 'bargain' ),
	            'section'    => 'colors',
	            'settings'   => 'bargain_secondary_color',
	            'description' => 'Alternate color. Used for buttons, stars, and accents.'
	        )
	    )
	);

	// add black color setting
	$wp_customize->add_setting(
	    'bargain_black_color',
	    array(
	        'default'     => '#282828'
	    )
	);
	//add secondary color control
	$wp_customize->add_control(
	    new WP_Customize_Color_Control(
	        $wp_customize,
	        'bargain_black_color',
	        array(
	            'label'      => __( 'Black Color', 'bargain' ),
	            'section'    => 'colors',
	            'settings'   => 'bargain_black_color',
	            'description' => 'Body text color and navigation background'
	        )
	    )
	);

	// add charcoal color setting
	$wp_customize->add_setting(
	    'bargain_charcoal_color',
	    array(
	        'default'     => '#4b4b4b'
	    )
	);
	//add charcoal color control
	$wp_customize->add_control(
	    new WP_Customize_Color_Control(
	        $wp_customize,
	        'bargain_charcoal_color',
	        array(
	            'label'      => __( 'Charcoal Color', 'bargain' ),
	            'section'    => 'colors',
	            'settings'   => 'bargain_charcoal_color',
	            'description' => 'Background color of darkened headings.'
	        )
	    )
	);

	// add darkgrey color setting
	$wp_customize->add_setting(
	    'bargain_darkgrey_color',
	    array(
	        'default'     => '#7d7d7d'
	    )
	);
	//add darkgrey color control
	$wp_customize->add_control(
	    new WP_Customize_Color_Control(
	        $wp_customize,
	        'bargain_darkgrey_color',
	        array(
	            'label'      => __( 'Dark Grey Color', 'bargain' ),
	            'section'    => 'colors',
	            'settings'   => 'bargain_darkgrey_color',
	            'description' => 'Dark border and divider color.'
	        )
	    )
	);

	// add grey color setting
	$wp_customize->add_setting(
	    'bargain_grey_color',
	    array(
	        'default'     => '#b5b5b5'
	    )
	);
	//add grey color control
	$wp_customize->add_control(
	    new WP_Customize_Color_Control(
	        $wp_customize,
	        'bargain_grey_color',
	        array(
	            'label'      => __( 'Grey Color', 'bargain' ),
	            'section'    => 'colors',
	            'settings'   => 'bargain_grey_color',
	            'description' => 'Primary background color.'
	        )
	    )
	);

	// add page color setting
	$wp_customize->add_setting(
	    'bargain_page_color',
	    array(
	        'default'     => ''
	    )
	);
	//add page color control
	$wp_customize->add_control(
	    new WP_Customize_Color_Control(
	        $wp_customize,
	        'bargain_page_color',
	        array(
	            'label'      => __( 'Page Color', 'bargain' ),
	            'section'    => 'colors',
	            'settings'   => 'bargain_page_color',
	            'description' => 'Page content default background color instead of image'

	        )
	    )
	);

	// add lightgrey color setting
	$wp_customize->add_setting(
	    'bargain_lightgrey_color',
	    array(
	        'default'     => '#e9e9e9'
	    )
	);
	//add lightgrey color control
	$wp_customize->add_control(
	    new WP_Customize_Color_Control(
	        $wp_customize,
	        'bargain_lightgrey_color',
	        array(
	            'label'      => __( 'Light Grey Color', 'bargain' ),
	            'section'    => 'colors',
	            'settings'   => 'bargain_lightgrey_color',
	            'description' => 'Light border and divider color.'
	        )
	    )
	);

	// add white color setting
	$wp_customize->add_setting(
	    'bargain_white_color',
	    array(
	        'default'     => '#ffffff'
	    )
	);
	//add white color control
	$wp_customize->add_control(
	    new WP_Customize_Color_Control(
	        $wp_customize,
	        'bargain_white_color',
	        array(
	            'label'      => __( 'White Color', 'bargain' ),
	            'section'    => 'colors',
	            'settings'   => 'bargain_white_color',
	            'description' => 'Content background color when there is no background image. Also used for white text.'

	        )
	    )
	);

	// add header locations icon setting
	$wp_customize->add_setting( 'bargain_location', array(
	'default' => '',
	'type' => 'theme_mod',
	'capability' => 'edit_theme_options',
	'transport' => '',
	) );
	// add header location icon control
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'bargain_location', array(
	    'label'    => __( 'Location Icon', 'bargain' ),
	    'section'  => 'header',
	    'settings' => 'bargain_location',
	) ) );

	// add header call icon setting
	$wp_customize->add_setting( 'bargain_reports', array(
	'default' => '',
	'type' => 'theme_mod',
	'capability' => 'edit_theme_options',
	'transport' => '',
	) );
	// add header call icon control
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'bargain_reports', array(
	    'label'    => __( 'Reports Icon', 'bargain' ),
	    'section'  => 'header',
	    'settings' => 'bargain_reports',
	) ) );

}
add_action( 'customize_register', 'bargain_customize_register' );