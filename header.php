<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Bargain_2
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
<?php get_template_part('customizer-output'); ?>

</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'bargain' ); ?></a>

	<header id="masthead" class="mobile-header" role="banner">
		
		<div class="site-branding header-background">
			<div class="logo-container">
				<?php if ( get_theme_mod( 'bargain_logo' ) ) : ?>
			    <div class="logo">
			        <a href='<?php echo esc_url( home_url( '/' ) ); ?>' title='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>' rel='home'><img src='<?php echo esc_url( get_theme_mod( 'bargain_logo' ) ); ?>' alt='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>'>
			        <?php if(get_theme_mod('bargain_logo_tagline') == '1') : ?>
			        <span class="site-name">SITENAME HERE</span>
			        <?php else : ?>
			        <?php endif; ?>
			        </a>
			    </div>
				<?php else : ?>
				<?php endif; ?>
				<a href="#" class="toggle-panel"><i class="fa fa-bars"></i></a>
			</div>
			<div id="panel-example">
			    <nav id="site-navigation" class="mobile-navigation nav-background" role="navigation">
					<?php wp_nav_menu( array( 'theme_location' => 'mobile', 'menu_id' => 'mobile-menu' ) ); ?>
				</nav><!-- #site-navigation -->
			</div>

		
	</header><!-- #masthead -->

	<header id="masthead" class="site-header" role="banner">
		
		<div class="site-branding container">
			<div class="pay-icon col-third">
				<?php if ( get_theme_mod( 'bargain_location' ) ) : ?>
				        <a href='/storage-facilities/' target="_blank" title='Find Location'><img src='<?php echo esc_url( get_theme_mod( 'bargain_location' ) ); ?>'></a>
						<span class="text">Find Location</span>
					<?php else : ?>
				<?php endif; ?>
			</div>

			<?php if ( get_theme_mod( 'bargain_logo' ) ) : ?>
		    <div class="logo col-third">
		        <a href='<?php echo esc_url( home_url( '/' ) ); ?>' title='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>' rel='home'>
		        	<img src='<?php echo esc_url( get_theme_mod( 'bargain_logo' ) ); ?>' alt='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>'>
			        <?php if(get_theme_mod('bargain_logo_tagline') == '1') : ?>
			        <?php else : ?>
			        <?php endif; ?>
		        </a>
		    </div>
			<?php else : ?>
		    <div class="col-third">
		        <h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
		        <p class="site-description"><?php bloginfo( 'description' ); ?></p>
		    </div>
			<?php endif; ?>

			<div class="call-icon col-third last">
			<?php if ( get_theme_mod( 'bargain_reports' ) ) : ?>
				        <a href='/dashboard/' title='Reports Login'><img src='<?php echo esc_url( get_theme_mod( 'bargain_reports' ) ); ?>'></a>
						<span class="text">Reports Login</span>
					<?php else : ?>
				<?php endif; ?>
			</div>
		</div><!-- .site-branding -->

		<nav id="site-navigation" class="main-navigation" role="navigation">
			<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu', 'menu_class' => 'menu' ) ); ?>
		</nav><!-- #site-navigation -->
	</header><!-- #masthead -->

	<div id="content" class="site-content">
